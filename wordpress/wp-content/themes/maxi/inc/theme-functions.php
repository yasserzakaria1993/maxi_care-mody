<?php

function maxi_scripts_custom() {
    wp_enqueue_style( 'custom-style', get_template_directory_uri().'/assets/style.css' );

    wp_enqueue_script( 'maxi-navigation', get_template_directory_uri() . '/assets/bundle.js', array(), '20151215', true );

}
add_action( 'wp_enqueue_scripts', 'maxi_scripts_custom' );


// Delete "Archive" , "Category" word

function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'my_theme_archive_title' );