<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Maxi
 */

?>
<div class="side-bar">
    <img alt="" class="side-bar-logo" src="./assets/img/maxicare_logo.png">
    <div class="side-bar-items">
       <?php
$args = array(
   'public'   => true,
   '_builtin' => false
);

$output = 'objects'; // 'names' or 'objects' (default: 'names')
$operator = 'and'; // 'and' or 'or' (default: 'and');
       $post_types = get_post_types( $args, $output, $operator );
        $loop_no=1;
       foreach ( $post_types  as $post_type ) {
       ?>

        <div class="side-bar-item hover-arrow <?php if($loop_no==1){echo "active";}?>">
            <div class="icon-and-text badge-number floating-number" data-number="2">
                <i class="fas fa-users icon"></i>
                <a href="<?php echo get_post_type_archive_link($post_type->name); ?>"><span class="text"><?php echo $post_type->label; ?></span></a>
            </div>
            <div class="icon-and-number badge-number" data-number="2">
                <i class="fal fa-angle-right arrow"></i>
            </div>
        </div>
<?php $loop_no++; } ?>
    </div>
</div>

