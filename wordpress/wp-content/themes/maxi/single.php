<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Maxi
 */

get_header();
?>
<div class="container">
    <div class="personal-and-address">
        <!--        text left and right     -->
        <div class="main-title">
            <div class="left-text">
                <i class="fal fa-angle-left icon"></i>
                <p class="text">
                    <span class="title"><?php the_title(); ?></span><br>
                    Birch Run, Michigan(MI), 48415 <br>
                    Customer ID No. 3521 <br>
                </p>
            </div>
            <div class="right-text">
                <span class="title">Last Update:</span>
                December 25, 2019, 8:25PM
            </div>
        </div>
        <!--       information and button      -->
        <div class="information-and-edit">
            <div class="info-and-address">
                <div class="personal-info">
                    <h3 class="text">Personal Information
                        <i class="fas fa-question-circle icon"></i>
                    </h3>
                    <table class="info">
                        <tr>
                            <td>Customer Name:</td>
                            <td>Raymond Romero</td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>(989) 4739-2893</td>
                        </tr>
                        <tr>
                            <td>Fax:</td>
                            <td>238-734-4829</td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>rayromero@gmail.com</td>
                        </tr>
                    </table>
                </div>
                <div class="address-info">
                    <h3 class="text">Address
                        <i class="fas fa-question-circle icon"></i>
                    </h3>
                    <table class="add">
                        <tr>
                            <td>Address:</td>
                            <td>206 Kooyong Road</td>
                        </tr>
                        <tr>
                            <td>Suburb:</td>
                            <td>Caulfield</td>
                        </tr>
                        <tr>
                            <td>Postcode:</td>
                            <td>3162</td>
                        </tr>
                        <tr>
                            <td>State:</td>
                            <td>CIV</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>United States</td>
                        </tr>
                    </table>
                </div>

            </div>
            <button class="main-button">Edit Info</button>
        </div>
    </div>
    <div class="contacts-and-facilities">
        <div class="the-date">
            <div class="date-header">
                contacts
                <i class="fas fa-minus-circle circle-icon"></i>
            </div>
            <table class="date-table">
                <thead>
                <tr>
                    <th><input type="checkbox"></th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Earl White</td>
                    <td>202-555-0155</td>
                    <td>ew.info@gmail.com</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Mary Campbell</td>
                    <td>202-555-0153</td>
                    <td>campbell_99@yahoo.com</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Rebecca Bailey</td>
                    <td>202-555-0143</td>
                    <td>rebecca_bailey@gmail.com</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                </tbody>
            </table>
            <div class="buttons">
                <button class="secondary-button">Edit</button>
                <button class="main-button">Add</button>
            </div>
        </div>
        <div class="the-date">
            <div class="date-header">
                Facilities
                <i class="fas fa-minus-circle circle-icon"></i>
            </div>
            <table class="date-table Facilities ">
                <thead>
                <tr>
                    <th><input type="checkbox"></th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Fax</th>
                    <th>Email</th>
                    <th>Contacts</th>
                    <th>Wings</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Rebecca Bailey</td>
                    <td>1817 N Fort Christmas Rd
                        Christmas, Florida(FL), 32709
                    </td>
                    <td>(407) 568-3181</td>
                    <td>202-555-0153</td>
                    <td>rebecca@yahoo.com</td>
                    <td>Ruth Peterson
                        516-410-0729
                        retersonruth@gmail.com
                    </td>
                    <td>Wings</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Matthew Brooks</td>
                    <td>10 Culvert St
                        Glens Falls, New York(NY), 12801
                    </td>
                    <td>202-555-0186</td>
                    <td>804-382-1004</td>
                    <td>matbrooks@yahoo.com</td>
                    <td>Catherine Joson
                        catherine001@gmail.com
                        203-295-5101
                    </td>
                    <td>Wings</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Bonnie Thompson</td>
                    <td>1719 Breezy Bend Dr
                        Katy, Texas(TX), 77494
                    </td>
                    <td>202-555-0135</td>
                    <td>202-555-0135</td>
                    <td>bonnie_23@gmail.com</td>
                    <td>Virginia Simmons
                        956-564-2028
                        simmons_V@gmail.com
                    </td>
                    <td>Wings</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>Christopher Perry</td>
                    <td>6707 Tudor Ln
                        Westmont, Illinois(IL), 60559
                    </td>
                    <td>804-382-1004</td>
                    <td>804-382-1004</td>
                    <td>perrychris@yahoo.com</td>
                    <td>Charles Gonzalez
                        770-471-6161
                        charlesgonz@gmail.com
                    </td>
                    <td>Wings</td>
                    <td><i class="far fa-ellipsis-v"></i></td>
                </tr>
                </tbody>
            </table>
            <div class="buttons">
                <button class="secondary-button">Edit</button>
                <button class="main-button">Add</button>
            </div>
        </div>
        <div class="products">
            <div class="text">Products</div>
            <i class="fas fa-plus-circle plus-icon"></i>
        </div>
    </div>
</div>
get_footer();
