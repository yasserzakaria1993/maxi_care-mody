<?php
/**
 * Template Name: Dashboard
 */
get_header();
?>


<!--      content -->
<div class="my-container">
    <div class="dashboard-head">
        <h3 class="text">Dashboard</h3>
        <div class="buttons">
            <button class="main-button">Refresh <i class="fas fa-sync icon"></i></button>
            <button class="main-button">Generate Report <i class="fas fa-download icon"></i></button>
        </div>
    </div>

    <div class="row">
        <div class="tapes customers col-3">
            <div class="text-number">
                <h3 class="text">Customers <span>+14 TODAY</span></h3>
                <div class="number">3,648</div>
            </div>
            <i class="fas fa-users icon"></i>
        </div>
        <div class="tapes earnings col-3">
            <div class="text-number">
                <h3 class="text">Earnings (Annual)</h3>
                <div class="number">$215,000</div>
            </div>
            <i class="fal fa-usd-circle icon"></i>
        </div>
        <div class="tapes notifications col-3">
            <div class="text-number">
                <h3 class="text">Notifications</h3>
                <div class="number">345</div>
            </div>
            <i class="fas fa-bell icon"></i>
        </div>
        <div class="tapes messages col-3">
            <div class="text-number">
                <h3 class="text">Messages</h3>
                <div class="number">12</div>
            </div>
            <i class="fas fa-comment-alt icon"></i>
        </div>

    </div>
    <section class="flow-chart">
        <div class="row">
            <div class="list col-4">
                <h3 class="title">Customer Earnings Report</h3>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Gerald Fields</h3>
                            <div class="text"><i class="fas fa-sort-up arrow-up icon"></i>$12,049</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Kenny Ruiz</h3>
                            <div class="text"><i class="fas fa-sort-up arrow-up icon"></i>$52,672</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Ashley Houston</h3>
                            <div class="text"><i class="fas fa-sort-down arrow-down "></i></i>$1,816</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Sherri Pearson</h3>
                            <div class="text"><i class="fas fa-sort-up arrow-up icon"></i>$63,836</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
            </div>
            <div class="list col-8"></div>
            <div class="list  col-4">
                <h3 class="title">Notifications
                    <span class="cartical">10 CRITICAL</span>
                    <span class="need-action">5 NEED ACTION</span></h3>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Adrian Jefferson</h3>
                            <div class="text">Aenean commodo ligula eget dolor. Aenean massa</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Diana Howell</h3>
                            <div class="text">Aenean commodo ligula eget</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Sherri Pearson</h3>
                            <div class="text">Aenean commodo ligula eget dolor aenean</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
                <div class="users">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Don Nelson</h3>
                            <div class="text">Aenean commodo ligula eget dolor</div>
                        </div>
                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
            </div>

            <div class="list col-4">
                <h3 class="title">Unread Messages</h3>
                <div class="users with-button ">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Gerald Fields</h3>
                            <div class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient.
                            </div>
                            <button class="main-button">Send Reply</button>
                        </div>
                    </div>
                    <i class="fal fa-angle-right arrow"></i>
                </div>
                <div class="users with-button">
                    <div class="icon-text">
                        <i class="fas fa-user-circle profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">Jaime Kelly</h3>
                            <div class="text">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel</div>
                            <button class="main-button">Send Reply</button>
                        </div>

                    </div>

                    <i class="fal fa-angle-right arrow"></i>

                </div>
            </div>

            <div class="list col-4">
                <h3 class="title">Urgent Action Needed</h3>
                <div class="users warning">
                    <div class="icon-text">
                        <i class="fal fa-map-marker-exclamation profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">backup schedule</h3>
                            <div class="text">Auto backup will start at 3:30 pm today</div>
                            <button class="secondary-button">Take Action Now</button>
                        </div>
                    </div>
                    <i class="fal fa-angle-right arrow"></i>
                </div>
                <div class="users warning">
                    <div class="icon-text">
                        <i class="fal fa-map-marker-exclamation profile-pc"></i>
                        <div class="left-content">
                            <h3 class="name">critical notification</h3>
                            <div class="text">10 notifications have labeled critical</div>
                        </div>
                    </div>
                    <i class="fal fa-angle-right arrow"></i>
                </div>
            </div>

        </div>
    </section>
</div>
<?php
get_footer();
?>
