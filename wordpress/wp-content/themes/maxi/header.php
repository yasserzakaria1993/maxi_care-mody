<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Maxi
 */

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>Main dashboard</title>
    <?php wp_head(); ?>
</head>
<body>
<div class="main-page-wrapper <?php if(is_single()){ echo "customer-page-wrapper"; }elseif(is_page('profile')){echo "profile-page-wrapper"; }else{ echo 'dashboard-page-wrapper'; } ?>">

 <?php get_sidebar(); ?>
    <!-- header and content   -->
    <div class="main-page-content">
        <div class="header">
            <div class="left-nav-items">
                <i class="fas fa-bars burger icon"></i>
                <div class="search">
                    <input placeholder="search" type="search">
                    <i class="far fa-search search-icon icon"></i>
                </div>
            </div>
            <div class="right-nav-items">
                <div class="icons">
                    <i class="fas fa-bell icon"></i>
                    <i class="fas fa-comment-alt comment icon">
                        <span class="dot"></span>
                    </i>
                    <i class="grid icon">
                        <span class="point"></span>
                        <span class="point"></span>
                        <span class="point"></span>
                        <span class="w-100"></span>
                        <span class="point"></span>
                        <span class="point"></span>
                        <span class="point"></span>
                        <span class="w-100"></span>
                        <span class="point"></span>
                        <span class="point"></span>
                        <span class="point"></span>
                    </i>
                </div>
                <div class="profile">
                    <div class="profile-pic">
                        <a href="<?php echo get_permalink(get_page_by_path('profile')); ?>"><img alt="" class="pic" src="<?php echo get_template_directory_uri(); ?>/assets/assets/img/joe.png"></a>
                    </div>
                    <span class="text">
                        <?php $current_user=wp_get_current_user(); ?>
                        <?php echo $current_user->display_name; ?></span>
                    <i class="fal fa-angle-down icon"></i>
                </div>
            </div>
        </div>
