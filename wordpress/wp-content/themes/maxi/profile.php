<?php
/**
 * Template Name: Profile
 */
get_header();
?>


<!--      content -->
<div class="my-container">
    <div class="row">
        <!--        left content -->
        <div class="col-3">
            <section class="left-content">
                <div class="profile-pic">
                    <img alt="" class="pic" src="../assets/img/joe.png">
                    <div class="text">
                        <?php $current_user=wp_get_current_user();
                        ?>
                        <?php echo $current_user->display_name; ?>
                    </div>
                </div>
                <div class="buttons">
                    <button class="secondary-button">Remove Profile</button>
                    <button class="main-button">Upload Profile</button>
                </div>
            </section>
        </div>
        <!--       right content   -->
        <div class="col-9">
            <div class="right-content">
                <!--              top content -->
                <section class="top-content">
                    <div class="row">
                        <div class="complete-information col-12">
                            <div class="main-title">Personal Information</div>
                        </div>
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">First Name</h3>
                                <div class="enter-date">
                                    <input placeholder="Joe" type="text">
                                </div>
                                <h3 class="text">This is inline help</h3>
                            </div>
                        </div>
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">Last Name</h3>
                                <div class="enter-date">
                                    <input placeholder="Donson" type="text">
                                </div>
                                <h3 class="text">This is inline help</h3>
                            </div>
                        </div>
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">Gender</h3>

                                <select class="soso" name="country">
                                    <option class="dodo" value=""></option>
                                    <option value="Algeria">Male</option>
                                    <option value="American Samoa">Femel</option>
                                </select>

                                <h3 class="text">This is inline help</h3>
                            </div>
                        </div>
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">First Name</h3>
                                <div class="enter-date">
                                    <input placeholder="" type="date">
                                </div>
                                <h3 class="text">This is inline help</h3>
                            </div>
                        </div>
                        <div class="complete-information col-12">
                            <div class="pay">
                                <h3 class="text">Membership</h3>
                                <div class="chose"><input name="how-pay" type="radio" value="Free">
                                    <span class="text">Free</span>
                                </div>
                                <div class="chose"><input name="how-pay" type="radio" value="Premium">
                                    <span class="text">Premium</span>
                                </div>
                            </div>
                        </div>
                        <div class="complete-information col-12">
                            <div class="main-title">Address</div>
                        </div>
                        <div class="complete-information col-12">
                            <div class="data">
                                <h3 class="head-text">Street</h3>
                                <div class="enter-date">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">City</h3>
                                <div class="enter-date">
                                    <input placeholder="Male" type="text">
                                </div>
                            </div>
                        </div>
                        <!--     put your State -->
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">State</h3>
                                <div class="enter-date">
                                    <input placeholder="" type="date">
                                </div>
                            </div>
                        </div>
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">Post Code</h3>
                                <div class="enter-date">
                                    <input placeholder="Male" type="text">
                                </div>
                            </div>
                        </div>
                        <!--    select gender-->
                        <div class="complete-information col-6">
                            <div class="data">
                                <h3 class="head-text">Country</h3>
                                <select class="your-country" name="country">
                                    <option class="dodo" value=""></option>
                                    <option value="Åland Islands">brazil</option>
                                    <option value="Albania">france</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">usa</option>
                                </select>
                            </div>
                        </div>
                        <div class="complete-information col-12">
                            <div class="main-title">Other Information</div>
                        </div>
                        <div class="complete-information col-12">
                            <div class="data">
                                <h3 class="head-text">Upload Files</h3>
                                <input class="d-none" id="file" type="file"/>
                                <button class="upload-file enter-date" name="button" value="Upload">
                                    <h3 class="text" id="dodo"></h3>
                                    <i class="fas fa-upload icon"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </section>
                <!--              middle content -->
                <section class="middle-content">
                    <div class="complete-information fields">
                        <div class="row ">
                            <div class="col-12">
                                <div class="main-title">Fields</div>
                            </div>
                            <div class="col-6">
                                <div class="data">
                                    <h3 class="head-text">Normal</h3>
                                    <div class="enter-date">
                                        <input placeholder="First Name" type="text">
                                    </div>
                                    <h3 class="text">This is inline help</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="data">
                                    <h3 class="head-text">Clicked</h3>
                                    <div class="enter-date">
                                        <input placeholder="Doe" type="text">
                                    </div>
                                    <h3 class="text">This is inline help</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="data">
                                    <h3 class="head-text">Error State</h3>
                                    <div class="enter-date">
                                        <input placeholder="Name already exist" type="text">
                                    </div>
                                    <h3 class="text">This field has error.</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="data">
                                    <h3 class="head-text">Completed</h3>
                                    <div class="enter-date special ">
                                        <input placeholder="Joe Donson" type="text">
                                    </div>
                                    <h3 class="text">This is inline help</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--  button content-->
                <section class="button-content">
                    <div class="row">
                        <div class="col-12">
                            <div class="main-title">Buttons</div>
                        </div>

                        <div class="primary-secondary-buttons col-12 col-xl-6">
                            <div class="buttons">
                                <h3 class="text">Primary Buttons</h3>
                                <div class=" row button ">
                                    <div class="col-4">
                                        <button class="main-button">Normal</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button">Clicked</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button disabled">DIsabled</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button dark">Normal</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button dark">Clicked</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button gray-disabled">DIsabled</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="primary-secondary-buttons col-12 col-xl-6">
                            <div class="buttons">
                                <h3 class="text">Primary Buttons</h3>
                                <div class="row button">
                                    <div class="col-4">
                                        <button class="secondary-button">Normal</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button">Clicked</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="secondary-button secondary-button-disabled">DIsabled</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="secondary-button secondary-button-dark">Normal</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="main-button dark ">Clicked</button>
                                    </div>
                                    <div class="col-4">
                                        <button class="secondary-button secondary-button-gray-disabled">DIsabled</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <!--              tapes-->
                <section class="tapes-content">

                    <div class="tapes-header">
                        <div class="tap-header active">Tab 01</div>
                        <div class="tap-header">Tab 02</div>
                        <div class="tap-header">Tab 03</div>
                        <div class="tap-header">Tab 04</div>
                        <div class="tap-header">Tab 05</div>
                    </div>
                    <div class="content"></div>
                </section>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
