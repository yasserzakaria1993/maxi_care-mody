export const dashboard = () => {
  
  
  $(".users").on("click", function () {
    $(this).toggleClass("active");
    
  });
  
  let ctx = document.getElementById("myChart").getContext("2d");
  let chart = new Chart(ctx, {
    // The type of chart we want to create
    
    type: "line",
    
    // The data for our dataset
    data: {
      
      labels: ["0", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22"],
      
      
      datasets: [
        
        {
          label: "My First dataset",
          backgroundColor: "rgba(0, 171, 132, 0.06)",
          borderColor: "#00ab84",
          data: [0, 5, 5, 10, 10, 15, 5, 5, 5, 0, 5, 5],
        },
        
        {
          label: "My second dataset",
          backgroundColor: "rgba(255,64,0,0.77)",
          borderColor: "#ff4000",
          data: [0, 0, 3, 2, 6, 4, 6, 3, 2, 3, 4],
        },
      
      
      ],
      
    },
    
    // Configuration options go here
    options: {
      // scales: {
      //   yAxes: [{
      //     type: "cmyScale",
      //     labels: ['$0.00', '$1,000', '$50,000', '$100,000', '$300,000']
      //   }],
      //   xAxes: [{
      //     type: "category",
      //     labels: ['0', '4', '6', '8', '10', '12', '14', '16', '18', '20', '22'],
      //
      //     // labels: ['$0.00', '$1,000', '$50,000', '$100,000', '$300,000']
      //   }]
      // },
      //
      
      // scales: {
      //   xAxes: [{ display: true }],
      //   yAxes: [{
      //     // type: 'myScale',
      //     // labels: ['$0.00', '$1,000', '$50,000', '$100,000', '$300,000'],
      //   }]
      // }
      //
    },
    
    
  });
  
  
};
