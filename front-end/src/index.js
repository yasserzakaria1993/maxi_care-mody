import "normalize.css/normalize.css";

import "./styles/style.scss";
import "@fortawesome/fontawesome-pro/scss/fontawesome.scss";
import "@fortawesome/fontawesome-pro/scss/light.scss";
import "@fortawesome/fontawesome-pro/scss/regular.scss";
import "@fortawesome/fontawesome-pro/scss/solid.scss";
import "@fortawesome/fontawesome-pro/scss/brands.scss";
import "slick-carousel/slick/slick.scss";
import {wayPoints} from "./scripts/general";
import {customer, dashboard, login, profile} from "./scripts/pages";
import {header} from "./scripts/layouts/header";
import "select2";


$(() => {
    $("select").select2();
    customer();
    profile();
    login();
    dashboard();
    header();
    wayPoints();
});