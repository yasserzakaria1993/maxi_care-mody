const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = function () {
    return {
        entry: path.resolve(__dirname, 'front-end/src/index.js'),
        mode: 'development',
        
        output: {
            // filename: '../wordpress/wp-content/themes/maxi-care/js/bundle.js',
            filename: 'bundle.js',
            path: path.resolve(__dirname, './front-end/public'),
            publicPath: './', // ModySwitch
            // publicPath: '/',
        },
        watch: true,
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/env'],
                            plugins: [
                                '@babel/plugin-proposal-class-properties',
                                '@babel/plugin-proposal-object-rest-spread',
                                '@babel/plugin-proposal-export-default-from',
                                '@babel/plugin-proposal-export-namespace-from',
                            ],
                        },
                    },
                }, {
                    test: /\.s?css$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                // hmr: process.env.WEBPACK_MODE === 'development',
                            },
                        },
                        {loader: 'css-loader', options: {importLoaders: 1, sourceMap: true}},
                        {loader: 'postcss-loader', options: {}},
                        {loader: 'sass-loader', options: {sourceMap: true}},
                    ],
                }, {
                    test: /\.(woff(2)?|ttf|eot|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            publicPath: function (url) {
                                return './' + url;
                            },
                            name: 'assets/[folder]/[name].[ext]',
                        },
                    }],
                }, {
                    test: /node_module.*\.(png|svg|jpg|gif)$/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            publicPath: function (url) {
                                return './' + url;
                            },
                            name: 'assets/[folder]/[name].[ext]',
                        },
                    }],
                }, {
                    test: /\.(png|svg|jpg|gif)$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            publicPath: function (url) {
                                return './' + url;
                            },
                            name: "assets/[folder]/[name].[ext]",
                            context: path.resolve(__dirname, './front-end/src/html'),
                        },
                    }],
                },
            ],
        },
        optimization: {
            sideEffects: false,
            minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
        },
        plugins: [
            // new webpack.SourceMapDevToolPlugin({
            //     filename: '[file].map',
            //     exclude: ['/vendor/'],
            // }),
            new MiniCssExtractPlugin({
                filename: 'style.css',
                publicPath: './',
            }),
            new HtmlWebpackPlugin({
                filename: "./index.html",
                template: path.resolve(__dirname, "./front-end/src/index.html"),
            }),
            new HtmlWebpackPlugin({
                filename: './customer.html',
                template: path.resolve(__dirname, './front-end/src/html/customer.html'),
            }),
            new HtmlWebpackPlugin({
                filename: "./profile.html",
                template: path.resolve(__dirname, "./front-end/src/html/profile.html"),
            }),
            new HtmlWebpackPlugin({
                filename: "./login.html",
                template: path.resolve(__dirname, "./front-end/src/html/login.html"),
            }),
            new HtmlWebpackPlugin({
                filename: "./dashboard.html",
                template: path.resolve(__dirname, "./front-end/src/html/dashboard.html"),
            }),
    
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
            }),
        ],
        // devtool: 'source-map', // ModySwitch
        devtool: 'cheap-module-eval-source-map',
        devServer: {
            publicPath: "/",
            contentBase: path.resolve(__dirname, './front-end/public'),
            historyApiFallback: true,
        },
    };
};